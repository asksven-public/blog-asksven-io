# !bin/bash

if [ "$1" == "" ]; then
  echo "local-runner.sh requires the job-name as argument"
  exit 1
fi  

source setenv.sh
source set-runner-env.sh

cd .. && gitlab-runner exec docker \
    --env CI_COMMIT_REF_SLUG=${CI_COMMIT_REF_SLUG} \
    --env CI_PROJECT_NAME=${CI_PROJECT_NAME} \
    --env DOCKER_REGISTRY_USER=${DOCKER_REGISTRY_USER} \
    --env DOCKER_REGISTRY=${DOCKER_REGISTRY} \
    -env DOCKER_REGISTRY_PASSWORD=${DOCKER_REGISTRY_PASSWORD} \
    "$1"
