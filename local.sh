#!/bin/bash
if [ -z "$CI_BUILD_REF" ]; then
  CI_BUILD_REF="new"
fi

# 1. generate static content
docker run --publish-all -e HUGO_BASEURL="https://blog.asksven.io" -v $(pwd)/source:/src -v $(pwd)/public:/output jojomi/hugo:0.69.0


#2. Build
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes

export DOCKER_CLI_EXPERIMENTAL=enabled

docker buildx rm container-builder
docker buildx create --platform "linux/amd64,linux/arm64,linux/arm/v7" --name container-builder --use

docker buildx build --platform "linux/amd64,linux/arm64,linux/arm/v7" -t asksven/blog.asksven.io:$CI_BUILD_REF . --push


# 3. run
echo to run the container
echo docker run -p 80:80 asksven/blog.asksven.io:${CI_BUILD_REF}
