# better.asksven.org Hugo static website

## run hugo without locally installed hugo

`docker run --publish-all -v $(pwd)/src:/src -v $(pwd)/public:/output jojomi/hugo:0.53`

## Build container

We have switched to multi-arch builds, that are run by `./local.sh`

## Run container locally

`docker run -p 80:8080 asksven/blog.asksven.io:latest`

## Run tests locally

1. Run container: `docker run -it -v $(pwd):/usr/workspace joyzoursky/python-chromedriver:3.8-selenium /bin/bash`
1. Set `BASE_URL` env var: `export BASE_URL="https://blog-stage.asksven.io"`
1. Go to directory: `cd /usr/workspace/webtests_1`
1. Run tests: `python -u selenium-python.py`

## Container security scan

```
docker run --rm -v ~/temp/trivvy:/root/.cache/ -v /var/run/docker.sock:/var/run/docker.sock aquasec/trivy --severity HIGH,CRITICAL --vuln-type os --ignore-unfixed asksven/blog.asksven.io:4ad66c1fd147d433cfb562aa32668606452228f4
```
