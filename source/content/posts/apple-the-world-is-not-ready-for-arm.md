+++
title =  "Apple! the world is not ready for ARM"
tags = ["docker", "arm"]
categories = ["docker"]
draft = false

date = "2020-06-29"
+++


## Introduction

Last week Apple announced its transition to ARM, err, [Apple silicon](https://www.apple.com/newsroom/2020/06/apple-announces-mac-transition-to-apple-silicon/).

My first thought was: poor developers who work with Docker! They will now develop and test on an architecture that is - at most - exotic in the datacenters of the mainstream cloud-providers.

Don't get me wrong, I am not an Apple-hater: my day-job work-horse is a 2015 Macbook Pro, and I like it better than my work Windows laptop, also because it has an I7 Intel CPU that has aged well. I am mostly into containers these days, so this post is an attempt to explain what my daily work and private projects would look like, if I were to use am ARM device to work on different Kubernetes-hosted projects.

## Workflow

Yes, I know, we have CI/CD so why should I care? Well, have you heard of the [inner loop](https://mitchdenny.com/the-inner-loop/)? I want to be productive so I need a local environment allowing me to run as much as possible locally, without compromises, to avoid the "it works on my machine" syndrome. So let's go through the pretty simple tooling I need:

1. [kubectl](https://kubernetes.io/de/docs/tasks/tools/install-kubectl/)
1. [Minikube](https://kubernetes.io/de/docs/tasks/tools/install-minikube/), or - even better - [k3d](https://github.com/rancher/k3d)
1. [Helm](https://github.com/helm/helm/releases)

### Kubectl

Let's start with the obvious: the documentation. Well, guess what? It is for `amd64`!

You can go download the [source code](https://github.com/kubernetes/kubectl/releases/tag/v0.18.5) and try to build it from source, but seriously!

But wait! There are [other sources](https://kubernetes.io/docs/setup/release/notes/)! Check!!!

### Minikube

The documentation does not mention anything about ARM: replacing `amd64` with `arm64` (`curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-arm64`) gave me a binary but I doubt it will run with anything except `vm-driver=none` as of today, since [Virtual Box does not run on ARM](orts a vm-driver=podman).

### K3D

Good news: [k3d runs on ARM](https://github.com/rancher/k3d/releases/tag/v1.7.0)

### Helm

Good news as well, [helm runs on ARM](https://github.com/helm/helm/releases/tag/v2.16.9) 

### Conclusion

I would be ready to take the challenge to move my inner-loop to ARM! Because I like challenges, am not under high pressure to produce code, and do not have a problem with cognitive-load since I do not have to produce code in my day-job. If I had to use the same stack, and working mainly as a dev, I would certainly not want to switch stacks just because Apple decided that it's best for me.

## Docker images

Now, imagine I took all the whoops and have an up-and-running ARM inner-loop. Now what? I certainly do not write Dockerfiles `FROM scratch` so I need sone base images.

Let's look into two scenarios:

1. My private stack
1. The popular list from [Docker Hub](https://hub.docker.com/search?type=image)

### How to check your stack?

You can check if *your favorite base image goes here* is supported on ARM:

```bash
export DOCKER_CLI_EXPERIMENTAL=enabled
docker manifest inspect nginx:1.15-alpine | grep architecture
```

This will either return nothing, or a subset from:

```bash
"architecture": "amd64",
"architecture": "arm",
"architecture": "arm64",
"architecture": "386",
"architecture": "ppc64le",
"architecture": "s390x"
```

### My private stack

Agreed, this is subjective! And I must admit, since I run my apps both on an amd64 and an arm (mixed v7 and 64 bits) Kubnertes cluster I make my choices based on what is available.

| Docker image | Runs on ARM? |
|------------------------|:-------------------:|
| gitlab-runner          | YES                 |
| Azure CLI              | NO                  |
| Grafana                | YES                 |
| Grafana Image Renderer | NO                  |
| Prometheus             | NO                  |
| Loki                   | YES                 |
| Postgres               | YES                 |
| Traefik                | YES                 |
| Ubuntu                 | YES                 |
| Alpine                 | YES                 |
| MongoDB                | YES                 |

Except the Azure CLI and the Grafana Image Renderer I must admit that I am pretty happy about what I am able to run on ARM (my Cluster, not my inner-loop). For the two mentioned "NO" I was able to build them for ARM, because I like challenges and because I am a nerd. But again, in my day-job I am under no pressure to deliver, so I am ready to invest plenty of time to reach my goals.  

### Popular stack

Whether it's my cup of tea or not does not matter: these are the most popular Docker images from Dockerhub:

| Docker image | Runs on ARM? |
|-------------------------------------------------|:-------------------:|
| [Oracle Database Enterprise Edition](https://hub.docker.com/_/oracle-database-enterprise-edition)               | NO                  |
| [Oracle Weblogic Server](https://hub.docker.com/_/oracle-weblogic-server-12c)               | NO               |
| [MySQL Server Enterprise](https://hub.docker.com/_/mysql-enterprise-server)               | NO               |
| [Couchbase](https://hub.docker.com/_/couchbase) | NO                  |
| [Postgres](https://hub.docker.com/_/postgres)   | YES                 |
| [Ubuntu](https://hub.docker.com/_/ubuntu)       | YES                 |
| [Redis](https://hub.docker.com/_/redis)         | YES                 |
| [Traefik](https://hub.docker.com/_/traefik)     | YES                 |
| [Node](https://hub.docker.com/_/node)           | YES                 |
| [MySQL](https://hub.docker.com/_/mysql)         | NO                  |
| [Busybox](https://hub.docker.com/_/busybox)     | YES                 |
| [Alpine](https://hub.docker.com/_/alpine)       | YES                 |

The caveat 


## Conclusion

Dear Apple, you are trying to sell me a solution that may fix your problem (not being dependent on Intel), but with such a transition you are making your problem to my problem: I say NO!
