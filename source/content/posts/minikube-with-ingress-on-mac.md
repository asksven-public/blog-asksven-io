+++
title =  "Minikube with ingress controller on the mac"
tags = ["kubernetes", "docker"]
categories = ["kubernetes"]
draft = false

date = "2023-03-19"
+++


## Introduction


## Update and install minikube

1. make sure that you have the most up-to-date version installed: `minikube update-check`. At the time of writing it's `v1.29.0`
1. delete any previous minikube config: `minikube delete`
1. install minikube in docker on the stable version: `minikube start --driver=docker --kubernetes-version=v1.26.1`. Note after the initial install you only need to run `minikube start` since the config is sticky
1. check that minikube is up-and-running: `kubectl get nodes`

The output should look like this:

```
NAME       STATUS   ROLES           AGE   VERSION
minikube   Ready    control-plane   29s   v1.26.1
```

## Enable the ingress controller addon

Run `minikube addons enable ingress`. Note the following mention:

```
After the addon is enabled, please run "minikube tunnel" and your ingress resources would be available at "127.0.0.1"
```

This is the main difference between running minikube on linux and on the mac: on linux the ingress controller gets expose under its own IP (`minikube ip`). Since the docker bridge behaves differently on the mac the ingress controller listens on localhost and needs to be tunneled to, by running `minikube tunnel` in a separate terminal.

## Deploy a simple app

1. Edit your `/etc/hosts` file and add `127.0.0.1 status.asksven.local` (or any other URL you go for)

2. Download [this](https://gist.github.com/asksven/d3ebdd47d80dd15ec9d81c77206f919d) gist, to let's say `~/statuspage`

3. Change the hostname in `ingress.yaml` depending on your choice in 1, and deploy: `kubectl create ns statuspage && kubectl -n statuspage apply -f <directory-where-you-unzipped-the-gist>`

4. Point your browser at `https://<the-url-you-chose-in-1>

Voila!



