+++
title =  "Gitlab CI/CD docker builds with docker 19.03 images"
tags = ["docker","cicd", "gitlab"]
categories = ["docker"]
draft = false

date = "2020-06-15"
+++


## Introduction

In this [previous post](/posts/docker-build-for-multiple-architectures-with-docker-buildx) I came accross an issue that I wanted to write about in more details:

- Why it is bad to rely on any kind of `latest` tags
- How docker `19.03-dind` will break your gitlab-ci docker builds and what you can do about it

If you do not use `latest` your pipeline is not already broken but this may still be interesting for you since this summary will help you update.

## Why latest is bad

We have all been told that relying on `latest` tags is bad: there may be breaking changes.

Well, `docker:19.03.11` and `docker:19.03.11-dind` came with such a breaking change (in fact, it affects 18.09+), and if you have not been careful, your pipeline is probably broken. 

## What are the changes?

The changes affect the default behavior of dind for version after 18.09: in these versions TLS has been made the default where it was not before. More about this in the "TLS" section [here]](https://hub.docker.com/_/docker?tab=description&page=1&name=dind) 

## Solution

Our build job looks like this, where the tricky details are in bold, and marked with a number, e.g. (1) that are not part of the job's source code, but rather markers for easier reading:

<pre>
docker build:
  stage: build
  image: docker:19.03.11
  tags:
    - asksven-homelab-prd-public
  services:
    - name: docker:19.03.11-dind
  variables:
    <b>(1)DOCKER_TLS_CERTDIR: ""</b> # set this to disable TLS (default on docker 18.09+)  
    <b>(2)DOCKER_HOST: tcp://localhost:2375/</b>
    DOCKER_DRIVER: overlay2

  script:
    - docker login -u "${DOCKER_REGISTRY_USER}" -p "$DOCKER_REGISTRY_PASSWORD" ${DOCKER_REGISTRY}
    # build and push
    - docker build -t ${DOCKER_REGISTRY}/${DOCKER_IMAGE_URL}:${CI_BUILD_REF} .
</pre>

1. We must set `DOCKER_TLS_CERTDIR` to empty in order to disable dind to go for the new TLS-default
1. If you the gitlab-runner on Kubernetes - as privileged pod - `DOCKER_HOST` must be set to point to the underlying dind
