+++
title =  "GPG and SSH with yubikey"
tags = ["yubikey",]
categories = ["security",]
draft = true

date = "2020-05-11"
+++


## Introduction

In [this](https://blog.asksven.io/posts/ssh-login-with-yubikey/) article we followed a few simple steps to configure a yubikey for the single purpose of holding a private SSH key. 

In this second post I will walk you through a better way to use your yubikey, together with GPG. This will allow us to - among other - manage SSH keys.

A very detailed guide can also be found [here](https://github.com/drduh/YubiKey-Guide).

## Setup your yubikey

## Setup your yubikey for GPG

### Generating keys


### Backup keys

As the next step will be moving the GPG keys to the yubikey we need to back them up first. 

**Note:** transfering GPG key to the yubikey is a one way street: they can not be read back out. We need a backup to being able to create suplicates of you yubikey's GPG keys.

### Transfer 

1. List your private keys: `gkg -K`
1. Export the private key ID to an environment variable: `export KEYID=<your-key-goes-here>`
1. Enter edit-mode: `gpg --edit-key $KEYID` (should show "Secret key is available", followed by four keys with "usage" = `SC`, `E`=Encryption, `A`=authentication and `S`=signing)

We will now select each key (with the commands `key 1`, `key 2` and `key 3`), transfer them to the according slot on the yubikey, with the `keytocard` command, and unselect the key again by re-entering `key n`. When a key is toggled a "*" is displays in the left column. Each command must be confirmed with the GPG passphrase:

- Signature key: into slot 1
- Encryption key: into slot 2
- Authentication key: into slot 3

Finally save and quit by entering `save`.

Verify with `gpg -K` and check that the keys 1..3 are marked as tranfered, with "ssb>"

### Set ultimate trust

Since these are your keys, you should set the trust to ultimate:

1. `gpg --edit-key $KEYID`
1. Enter `trust`
1. Select "5" (I trust ultimately)
1. `quit` 

### Verify that GPG works

1. Remove and re-insert the yubikey, and enter `gpg --card-status`
1. Encrypt a message: `echo "test message string" | gpg --encrypt --armor --recipient $KEYID_0 --recipient $KEYID_1 --recipient $KEYID_2 -o encrypted.txt`
1. Decrypt the message (will ask you for your PIN): `gpg --decrypt --armor encrypted.txt`
1. Sign a message: `echo "test message string" | gpg --armor --clearsign > signed.txt`
1. Verify signature: `gpg --verify signed.txt`

## Setup for SSH

We will use `gpg-agent`, that supports the SSH protocol.

### gpg.conf

You `~/.gnupg/gpg.conf` should have at least the following lines:

```
enable-ssh-support
default-cache-ttl 60
max-cache-ttl 120
pinentry-program /usr/bin/pinentry-curses
# Use this if you want windowed PIN entry
# pinentry-program /usr/bin/pinentry-gnome3
```

### .bashrc

Add the following to your `.bashrc`:

```bash
export GPG_TTY="$(tty)"
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent
```
### Get public key

Enter `ssh-add -L` to display the SSH public-key, and add it to your `~/.ssh`, e.g. to `id_rsa_yubikey.pub`.

Copy the key to your server: `ssh-copy-id -f -i ~/.ssh/id_rsa_yubikey.pub`

### Troubleshooting

In case you have multiple yubikeys you may end-up in situtation where gpg-egent requests you to provide another key. To fix this:

1. Get the yubikey ID: `gpg --with-keygrip --card-status` (Field `Application ID`)
1. 