+++
title =  "Self-Service Operations: the Why? and the How?"
tags = ["devops",]
categories = ["devops",]
draft = false

date = "2020-04-19"
+++

## Disclaimer

The opinions depicted in this post are mine, not the ones of my employer.

## Introduction

Self-service operations is a term coined by [Damon Edwards](https://www.linkedin.com/in/damonedwards/) from [Rundeck](https://www.rundeck.com) to describe principles (and tools) that should guide operations in an enterprise, or any other organization that have more than one [two pizza team](http://blog.idonethis.com/two-pizza-team/). Why self-service operations is so important comes from the fact, that in large organizations teams depend on other teams (because there is a limit to the size of a team and to what their responsibility can encompass). It is important to implement self-service operations at the interfaces,  to reduce the pain caused by backlog dependencies: by decoupling them with self-service.
Looking through the Site Reliability Engineering [lense](https://landing.google.com/sre/) toil describes the *effort spent touching systems during normal operations*, and is considered a [bug](https://landing.google.com/sre/sre-book/chapters/eliminating-toil/); reducing and limiting toil may be a good first step into self-service operations, starting in your team!

**But why should I care?**

The world around us is changing and our industry is facing new requirements; even if it took longer than elsewhere, these have made an entry in the enterprise as well:

- be more efficient and do more with less
- be faster in execution
- be able to react faster to changing requirements

If you don't feel this pressure I have bad news for you: you are not relevant anymore...

## Automate all the things!

Automation is part of the answer, the other parts being orchestration and interaction models. But if it were so simple how come this problem has still not be solved?

### Make conscious decisions about your work environment

Depending on the situation you are in, a few changes up-front may be required:

- you need to create a [culture of trust](https://www.mindtools.com/pages/article/cynefin-framework.htm), embracing experimentation and protected by psychological safety
- be clear about [what you measure](https://quotefancy.com/quote/888127/Peter-F-Drucker-You-can-t-manage-what-you-don-t-measure)
- think about [your incentives are](https://www.bridging-the-gap.com/beware-you-get-what-you-measure/)
- make **all** your [work visible](https://www.amazon.de/Making-Work-Visible-Exposing-Optimize/dp/1942788150). Any I am not talking about having a ticket system where you get your work from. **Don't kid yourself**, think about the work you do that is not triggered by these tickets!
- understand the [difference](https://twitter.com/graffretail/status/1093314909436366848) between *"doing the things right"* and *"doing the right things"*
- understand that improvement of a system requires dedicated time
- encourage your staff: I have had great results by simply encouraging team members to challenge whether what they do [create value](https://theleanway.net/The-Five-Principles-of-Lean), and to change the system they work in where ever they can. Make these improvements visible, and celebrate your successes. You can thank me later ;-)

### Automation requires dev skills

To implement automation you need:

- dev skills: any scripting language is a good start, be it Powershell, Python or any other language supporting modular code
- an understanding of building testable code: this is important if you want to release often and with a safety -net. This is often underestimated, leading to code staying for too long unreleased, because it contains too many unfinished changes
- an understanding of configuration and release management, for the above-mentioned reasons

If you work in operations in an enterprise, the chances are high that you are not staffed for engineering-work, but rather for executing tasks and following standard operating procedures. In that case, you **have a problem to solve!**
The good news is that individuals [thrive](https://medium.com/10x-curiosity/delivering-success-through-intrinsic-motivation-autonomy-mastery-and-purpose-d1a1ab02744e) for *autonomy, mastery and purpose*. So it is on you to [unlock](https://www.scaledagileframework.com/unlock-the-intrinsic-motivation-of-knowledge-workers/) this potential: you know your team best, and it should not be so hard to identify the individuals with the potential to be pioneers. Give them goals and the time, and you will be surprised by what they will achieve.

### Start with what you control

Starting on this journey, in the first step, it is all about the [circle of control](https://www.thensomehow.com/circles-of-influence/). Understanding what you can decide to change on your own is key. Avoiding having to get other units or your management on your side before you can start: start small and show results.
These will give you arguments to persuade whoever is in your circle of control. And believe me, it is all about how your results are presented and about pushing the right buttons. The bad news is that there are no recipes in regards to [what the right arguments are](https://www.askamanager.org/2013/11/8-ways-to-persuade-your-boss-to-say-yes.html).

### Don't wait!

In many enterprises there is one or more [ITSM software suites](https://www.cio.de/a/was-ist-was-bei-itil-und-itsm,3258078,2) already implemented or - even worse - planned for roll-out.
Don't wait for these to solve the problem, because a software selection has never solved a process problem. Even worse, automation is most often - at best - an afterthought, or  - at worst - not in scope. Waiting for a unified software to lay the groundstone enabling you to automate will most probably nEVER happen. Based on this you should start with the best idea you have, because [hope is not a strategy](https://medium.com/@jerub/hope-is-not-a-strategy-6a7d0a3b1c08). Automation is mostly about algorithms and integration and not about implementation, so you will create value by implementing **any automation**, even if you have to re-write it later for a better fit for purpose.

### Get to know your customers

After having kicked the tire with automation in your circle of control, it is time to have a broader look.

**First things first:** understand that you have a customer.

Once you start understanding what your customer needs, you can think about your contribution:

- stop doing things that do not add value
- identify things you do that add value, and start measuring them with KPI
- understand the end-to-end process: this is easier said than done and you may want to do [value stream mapping](https://en.wikipedia.org/wiki/Value-stream_mapping) to understand where your contribution is. If you don't, you may end-up optimizing something that is not the bottleneck, and fixing the wrong problem
- talk to your customer to better understand his needs, and the pain that he has being dependent on your backlog: this will help you prioritize your backlog
- create a backlog of actions to take to make your customer happier and to improve on your KPI

Talking to your customers also has interesting side-effects: you will better understanding their needs, and they will also understand your situation. This is the baseline for creating win-win situations, and you will be blown-away but the change in attitude, once you start collaborating along the value stream.

### Automation does not cause unemployment
 
Every change comes with the fear of the unknown, and automation (like Cloud) triggers fears that you need to understand, and deal with. In most enterprise ops teams, the individuals are I-shaped: there is one topic they are expert in. This is problematic at different levels:

- most modern ways of working - like DevOps - require [more T-shaped, and less I-shaped](https://devopsinstitute.com/2017/11/15/from-i-shaped-to-t-shaped-why-devops-professionals-need-to-be-multi-skilled/) professionals
- being I-shaped means that a person has invested a lot of energy specializing. Change shows a possibility, that this specialty is not as important anymore
- being I-shaped also means that the learning habits have focussed on keeping track of the development in a specific area. Developments in other areas have not been in focus, and switching from I-shaped to T-shaped sometimes triggers fears comparable with the [impostor syndrome](https://time.com/5312483/how-to-deal-with-impostor-syndrome/)
- being T-shaped means keeping up with a lot of things that you are not expert in. This also means constantly learning new things and understanding all the things that you don't know yet, and being fine with that.

So automation at the end is about culture as much as it is about technology, and that is the reason why this topic is [neither simple nor complicated: it is complex](https://www.mindtools.com/pages/article/cynefin-framework.htm)

