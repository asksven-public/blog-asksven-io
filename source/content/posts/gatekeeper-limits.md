+++
title =  "Kubernetes security policies with Gatekeeper"
tags = ["kubernetes","security"]
categories = ["Kubernetes","security"]
draft = true
date = "2020-05-16"
+++

## Introduction

This post is part of a serie of posts about [gatekeeper](/posts/gatekeeper),  aiming at implementin a replacement for pod security policies, by enforcing pod resource limits.

The other posts can be found here:

- [Introduction to Gatekeeper](/posts/gatekeeper)
- Contraint enforcing dropping NET_RAW
- Contraint enforcing read-only file system
- Contraint enforcing unprivileged pods
- Contraint enforcing running containers as non-root

Examples as well as the installation of Gatekeeper are on [github](https://github.com/asksven/gatekeeper).

## Prepare

Before we start we need to make sure that we control what happens, do not break anything or affect the cluster's availability. For that we will label all critical namespaces 

## Enforcing limits

### Create

1. `kubectl -n gatekeeper-system apply -f example-enforce-limits/templates/pod_limits_template.yaml`
1. `kubectl -n gatekeeper-system apply -f example-enforce-limits/contraints/pod_must_have_limits.yaml`

### Test

To test the policy we create a pod without limits:

`kubectl apply -f example-enforce-limits/resources/bad-pod.yaml` returns the following:

```bash
Error from server ([denied by pods-must-have-limits] container <web> has no resource limits): error when creating "example-enforce-limits/resources/bad-pod.yaml": admission webhook "validation.gatekeeper.sh" denied the request: [denied by pods-must-have-limits] container <web> has no resource limits
```

`kubectl apply -f example-enforce-limits/resources/good-pod.yaml` returns:

```
pod/web created
```

### Limitation

Now, let's create a deployment object instead of a pod:

`kubectl apply -f example-enforce-limits/resources/bad-deployment.yaml`

The command returns:

```bash
deployment.apps/webserver created
```

but looking at the deployment shows that there is something wrong:

```bash
$ kubectl get deploy
NAME        READY   UP-TO-DATE   AVAILABLE   AGE
webserver   0/1     0            0           39s
```

and `kubectl get deploy webserver -o yaml` shows the reason: 

```bash
$ kubectl get deploy webserver -o yaml
...
kind: Deployment
    message: 'admission webhook "validation.gatekeeper.sh" denied the request: [denied
      by pods-must-have-limits] container <blog> has no resource limits'
    reason: FailedCreate
```

The reason for that is that objects of type `Deployment` are not checked by any contraints. When Kubernetes handles the creation of the deployment, it in turn creates the pod, and fails silently. This kind of errors may be hard to catch so we need to find a way to add a contraint that is checked when the `Deployment` is created.

Fortunately the constraints and template for a `Deployment` are very similar to the ones from the `Pod`:

The contraint only differs in the `kinds` and `apiGroup`:

```bash
apiVersion: constraints.gatekeeper.sh/v1beta1
kind: K8sDeploymentLimits
metadata:
  name: deployments-must-have-limits
spec:
  match:
    kinds:
      - apiGroups: ["apps"]
        kinds: ["Deployment"]
```

The rego expressions in the `ConstraintTemplate` only differ in the way to get at the limits, using the path `input.review.object.spec.template.spec` instead of `input.review.object.spec`.

#### Fix

Let's create the new template and contraint:

1. `kubectl -n gatekeeper-system apply -f example-enforce-limits/templates/deployment_limits_template.yaml`
1. `kubectl -n gatekeeper-system apply -f example-enforce-limits/contraints/deployments_must_have_limits.yaml`

Now applying the bad deployment leads to what we wanted:

```bash
$ kubectl apply -f example-enforce-limits/resources/bad-deployment.yaml
Error from server ([denied by deployments-must-have-limits] container <blog> has no resource limits): error when creating "example-enforce-limits/resources/bad-deployment.yaml": admission webhook "validation.gatekeeper.sh" denied the request: [denied by deployments-must-have-limits] container <blog> has no resource limits
```