+++
title =  "Manage your home-network with an Azure DNS Zone"
tags = ["docker", "rpi", "arm"]
categories = ["docker", "rpi"]
draft = false

date = "2020-06-28"
+++


## Introduction

Azure DNS Zones is an inexpensive way to manage DNS records for your domains, even if you have dynamic IP.
Back in the days, and before greedy Oracle took it over, [dyndns.org](dyndns.org) used the (free) place to be if you had a dynamic IP and wanted to expose your home-network.

Things have changed but fortunately offers like [Azure DNS Zones](https://azure.microsoft.com/en-ca/pricing/details/dns/) are as inexpensive as a few Euro per month, and are easy to maintain with a little scripting. This article takes you through such a script, that can be run on your server, in a Docker container or even on Kubernetes.

## What the script does

It monitors your IP address and maintains a list of domains to be up-to-date: when your IP changes the script will detect it and update your DNS zone.

## Pre-requisites

1. Install the [Azure CLI](https://docs.microsoft.com/bs-latn-ba/cli/azure/install-azure-cli?view=azure-cli-latest)
1. Create an Azure DNS Zone
1. Create a service principal: `az ad sp create-for-rbac --name AcmeDnsUpdater`

## Setup

[This](https://github.com/asksven/azdnszone) repo contains the detailed instructions:

1. clone the repo
1. copy `setenv.template` to `setenv` and customize it
1. Schedule `update-ip.sh` to run in a scheduled manner

## Advanced use

If you don't want to install the software you can run the script as a Docker container, or as a Kubernetes cron-job instead.

### Docker

```bash
docker run -e STATELESS=1 -e TWELVE_FACTORS=1 -v $PWD:/config asksven/azdnszone:5 /bin/bash -c "/update-ip.sh config/setenv"
```

### Kubernetes cron-job

Runs on amd64 and on arm processors!

1. Configure `setenv`
1. Create a namespace: `kubectl create ns azdnszone`
1. Create a configmap: `source setenv && kubectl  -n azdnszone create configmap azdnszone-config --from-literal INITIALIZED=1 --from-literal appId=$appId --from-literal password=$password --from-literal tenant=$tenant --from-literal SUBSCRIPTION=$SUBSCRIPTION --from-literal REQUESTED_NAMES=$REQUESTED_NAMES --from-literal AZ_DNS_RG=$AZ_DNS_RG --from-literal PARENT_DOMAIN=$PARENT_DOMAIN --from-literal STATELESS=$STATELESS --from-literal TWELVE_FACTORS=$TWELVE_FACTORS`
1. Create a cron-job: `kubectl -n azdnszone apply -f kubernetes/`

### Challenges

The biggest challenge was multi-arch: Microsoft does not maintain non-amd64 images for the Azure CLI so I had to [build one](https://github.com/asksven/azure-cli). 