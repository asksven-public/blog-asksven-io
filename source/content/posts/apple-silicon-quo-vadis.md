+++
title =  "Apple Silicon, quo vadis?"
tags = ["docker", "arm"]
categories = ["docker"]
draft = false

date = "2021-01-16"
+++


## Introduction

Shame on me, I have not written anything for half a year!

What if I had been asleep for half a year, woke-up and realized: well, so much time has passed, the arm macs are now mainstream, have full-support of native tools making developers more efficient, right? Admittedly, I am not speaking for all developers, but I never stated I was.

My conclusion from the [last post](https://blog.asksven.io/posts/apple-the-world-is-not-ready-for-arm/) was: poor developers who work with Docker!

Now. let's revisit the findings from half a year ago, and also put some thoughts about what may have changed.

## Workflow


### Kubectl

The client binaries are listed [here](https://kubernetes.io/docs/setup/release/notes/#client-binaries)

**Result**: still no support for arm on the mac

### Minikube

The [Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/) release page mentions darwin but there seems to be no sign of mac ARM support. Even worse, this [issue](https://github.com/kubernetes/minikube/issues/9224) confirms that there is still no support for the M1 CPU

**Result**: still no support for arm on the mac

### k3d

The [latest](https://github.com/rancher/k3d/releases/tag/v3.4.0) release does not support anything else than darwin.  

**Result**: still no support for arm on the mac

### Helm

The [release](https://github.com/helm/helm/releases) page does not show any love either

**Result**: still no support for arm on the mac

## Overall view

Based on [this](https://news.ycombinator.com/item?id=25228786) post I understand that the suggestion is to forget about the inner-loop, and go for an outer loop instead. WTF!

On the inner-loop side of the road it looks like there may be [a light at the end of the tunnel](https://github.com/docker/roadmap/projects/1); Docker for desktop support, yay! It really looks like Apple was able to capture the focus and the energy of the whole industry to focus on making sure that Docker development is made available on the ARM macs!

### Conclusion

** Same as six month ago, I did not change a bit**

I would be ready to take the challenge to move my inner-loop to ARM! Because I like challenges, am not under high pressure to produce code, and do not have a problem with cognitive-load since I do not have to produce code in my day-job. If I had to use the same stack, and working mainly as a dev, I would certainly not want to switch stacks just because Apple decided that it's best for me.

## Docker images

Now, imagine I took all the hoops and have an up-and-running ARM inner-loop. Now what? I certainly do not write Dockerfiles `FROM scratch` so I need some base images.

Let's look into two scenarios:

1. My private stack
1. The popular list from [Docker Hub](https://hub.docker.com/search?type=image)

### How to check your stack?

You can check if *your favorite base image goes here* is supported on ARM:

```bash
export DOCKER_CLI_EXPERIMENTAL=enabled
docker manifest inspect nginx:1.15-alpine | grep architecture
```

This will either return nothing, or a subset from:

```bash
"architecture": "amd64",
"architecture": "arm",
"architecture": "arm64",
"architecture": "386",
"architecture": "ppc64le",
"architecture": "s390x"
```

### My private stack

Agreed, this is subjective! And I must admit, since I run my apps both on an amd64 and an arm (mixed v7 and 64 bits) Kubnertes cluster I make my choices based on what is available.

| Docker image | Runs on ARM? |
|------------------------|:-------------------:|
| gitlab-runner          | YES                 |
| Azure CLI              | NO                  |
| Grafana                | YES                 |
| Grafana Image Renderer | NO (except if you [build it on your own](https://github.com/asksven/grafana-image-renderer))                 |
| Prometheus             | YES                  |
| Loki                   | YES                 |
| Postgres               | YES                 |
| Traefik                | YES                 |
| Ubuntu                 | YES                 |
| Alpine                 | YES                 |
| MongoDB                | YES                 |

Except the Azure CLI I must admit that I am pretty happy with what I can run on ARM (my Cluster, not my inner-loop). For the mentioned "NO" I was able to build them for ARM, because I like challenges and because I am a nerd. But again, in my day-job I am under no pressure to deliver, so I am ready to invest plenty of time to reach my goals.  

### Popular stack

Whether it's my cup of tea or not does not matter: these are the most popular Docker images from Dockerhub:

| Docker image | Runs on ARM? |
|-------------------------------------------------|:-------------------:|
| [Oracle Database Enterprise Edition](https://hub.docker.com/_/oracle-database-enterprise-edition)               | NO                  |
| [Couchbase](https://hub.docker.com/_/couchbase)               | NO               |
| [Postgres](https://hub.docker.com/_/postgres)   | YES                 |
| [Traefik](https://hub.docker.com/_/traefik)     | YES                 |
| [Redis](https://hub.docker.com/_/redis)   | YES                 |
| [Ubuntu](https://hub.docker.com/_/ubuntu)   | YES                 |
| [Node](https://hub.docker.com/_/node)           | YES                 |
| [MySQL](https://hub.docker.com/_/mysql)         | NO                  |
| [Busybox](https://hub.docker.com/_/busybox)     | YES                 |
| [Alpine](https://hub.docker.com/_/alpine)       | YES                 |
| [MongoDB](https://hub.docker.com/_/mongo)       | YES                 |


## Conclusion

Dear Apple, it seems the tail does not wag with the dog after all! But I feel some progress on the data-center side; finding, building, and consuming docker images for ARM has become easier. But that's outer-loop and it does not count! Looking into higher layers of abstraction (helm) I could not observe any change: the default is still `X84-64` images.
