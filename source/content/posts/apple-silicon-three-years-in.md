+++
title =  "Apple Silicon, quo vadis?"
tags = ["docker", "arm"]
categories = ["docker"]
draft = true

date = "2023-07-30"
+++


## Introduction

It now has been three years in since my [first post](https://blog.asksven.io/posts/apple-the-world-is-not-ready-for-arm/) and two and hald years in since my [second post](https://blog.asksven.io/posts/apple-silicon-quo-vadis/).

I think it is time to see how things have evolved: is a laptop with am arm processor up to snuff if you are working near to the metal.

**Caution:** My situation is not your situation, and my conclusions may not apply to you!

I am lucky and have two (!) macbook pros at my disposal: one MBCB 16" with an i9 and 64GB of RAM, one 14" M1 with 64GB of RAM. 

## Summary

### Kubectl

The client binaries are listed [here](https://github.com/kubernetes/kubernetes/tree/master/CHANGELOG)

**Result**: good news! The binaries are now available for darwin and the `arm64` architecture. 

### Minikube

The [Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/) release page now nmention `arm64` as a first class citizen. Please note that Darwin is not linux and that there are some [specifics](https://blog.asksven.io/posts/minikube-with-ingress-on-mac/) to consider related to the ingress-controller.

### k3d

The [latest](https://github.com/k3d-io/k3d/releases/tag/v5.5.1) release now supports both `amd64` and `arm64` architectures.  

### Helm

The [release](https://github.com/helm/helm/releases) now show support of both `amd64` and `arm64` architectures.  

## Overall view

On the inner-loop side there have been many unrelated developments: in my eyes Docker fr Desktop is dead due to bad choices [in the licensing](https://www.docker.com/pricing/). On the good side there are free alternatives on the market:

- [Rancher Desktop](https://rancherdesktop.io/)
- [Colima (my favorite)](https://github.com/abiosoft/colima)

## Docker images

Now, imagine I took all the hoops and have an up-and-running ARM inner-loop. Now what? I certainly do not write Dockerfiles `FROM scratch` so I need some base images.

Let's look into two scenarios:

1. My private stack
1. The popular list from [Docker Hub](https://hub.docker.com/search?type=image): kudos to the maintainers: the TOP25 now all support `arm64`

### How to check your stack?

You can check if *your favorite base image goes here* is supported on ARM:

```bash
export DOCKER_CLI_EXPERIMENTAL=enabled
docker manifest inspect nginx:1.25.1-alpine | grep architecture
```

This will either return nothing, or a subset from:

```bash
"architecture": "amd64",
"architecture": "arm",
"architecture": "arm64",
"architecture": "386",
"architecture": "ppc64le",
"architecture": "s390x"
```

### My private stack

Agreed, this is subjective! And I must admit, since I run my apps both on an amd64 and an arm (mixed v7 and 64 bits) Kubnertes cluster I make my choices based on what is available.

| Docker image | Runs on ARM? |
|------------------------|:-------------------:|
| gitlab-runner          | YES                 |
| Azure CLI              | YES                  |
| Grafana                | YES                 |
| Grafana Image Renderer | NO (except if you [build it on your own](https://github.com/asksven/grafana-image-renderer))                 |
| Prometheus             | YES                  |
| Loki                   | YES                 |
| Postgres               | YES                 |
| Traefik                | YES                 |
| Ubuntu                 | YES                 |
| Alpine                 | YES                 |
| MongoDB                | YES                 |  

### Popular stack

Whether it's my cup of tea or not does not matter: these are the most popular Docker images from Dockerhub:

| Docker image | Runs on ARM? |
|-------------------------------------------------|:-------------------:|
| [Alpine](https://hub.docker.com/_/alpine)               | YES                  |
| [Nginx]https://hub.docker.com/_/nginx)               | YES               |
| [Busybox]https://hub.docker.com/_/busybox)   | YES                 |
| [Ubuntu](https://hub.docker.com/_/ubuntu)     | YES                 |
| [Pyhton](https://hub.docker.com/_/python)   | YES                 |
| [Redis](https://hub.docker.com/_/redis)   | YES                 |
| [Postgres](https://hub.docker.com/_/postgres)           | YES                 |
| [Node](https://hub.docker.com/_/node)         | YES                  |
| [Httpd](https://hub.docker.com/_/httpd)     | YES                 |
| [MongoDB](https://hub.docker.com/_/mongo)       | YES                 |
| [Memcached](https://hub.docker.com/_/memcached)       | YES                 |

## The inner loop

Yes I know, this is a pet peeve of mine: what does the M1 architecture and all its layers cost me in the inner feedback loop; remember, the inner loop should be optimized for the quickest feedback possible!


## Conclusion

Finding, building, and consuming docker images for `arm64` has become mainstream, three years in. But I must say I am not satisfied with the inner loop yet. Of course the M1/M2 architecture is great when it comes to battery life, performance, etc., 80% of the time. But then there are still these edge-cases:

- Building `amd64` images
- Running a VM (always `amd64`) on an M1 mac
