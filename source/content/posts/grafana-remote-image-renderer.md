+++
title =  "Grafana remote image renderer"
tags = ["docker", "kubernetes", "rpi", "arm"]
categories = ["docker", "rpi", "kubernetes"]
draft = false

date = "2020-06-20"
+++


## Introduction

Since the Grafana Image Renderer plug-in is not [supported anymore](https://grafana.com/blog/2020/05/07/grafana-7.0-preview-new-image-renderer-plugin-to-replace-phantomjs/) from Grafana 7.0 some changes are required to switch to the [remote image renderer](https://github.com/grafana/grafana-image-renderer/blob/master/docs/remote_rendering_using_docker.md), and run it as a docker container.

This post goes into the details of setting-up a remote image renderer for Kubernetes, on amd64, arm/v7 and arm64.

## Multi-arch build

The official [git repo](https://github.com/grafana/grafana-image-renderer/blob/master/docs/remote_rendering_using_docker.md) only supports linux/amd64 at this moment but there is an [issue](https://github.com/grafana/grafana-image-renderer/issues/7) for arm-support.

I have created a fork of the repo [here](https://github.com/asksven/grafana-image-renderer) with the changes required for building for arm/v7 and arm64.

1. Clone the grafana-image-renderer fork: `git clone https://github.com/asksven/grafana-image-renderer`
1. Login to the registry you are going to push to: `docker login`
1. Set environment variables: `export REPOSITORY=asksven/grafana-image-renderer && export VERSION=1`
1. Start the emulator: `docker run --rm --privileged multiarch/qemu-user-static --reset -p yes`
1. Build: `make docker-buildx`

### Deploy the image renderer on Kubernetes

Follow the instructions [here](https://github.com/asksven/grafana-image-renderer/tree/master/kubernetes) to deploy the remote image renderer, anc configure Grafana to use it.

