+++
title =  "Google: give us BATTERY_STATS back!"
tags = ["android","dev"]
categories = ["Mobile",]

date = "2013-11-23"
+++

## Note

I have saved this post from Google+ before its shutdown because I am still pissed at Google.

If you already have a device with Android 4.4 Kitkat on it you may have noticed that your favorite battery stats tool, whether it is BetterBatteryStats, GSam of wakelock detector, does not work.

Well it is not uncommon that new Android versions break a few apps and it usually takes us a few days for your favorite dev to fix things.
But this time is different and the changes are important enough for me to attempt to draw some attention on this matter.

## Some context
Android has a service called 'batteryinfo' that collects info about what goes on on your phone. That service is not perfect but provides some pretty good information to our battery monitoring apps: an insight into what drains the battery. 
To communicate with that service an app has (in fact had) just to claim the android.permission.BATTERY_STATS permission. This permission is (was) of the category "dangerous", notifying users of the fact that they should check why an app requires that permission. For a battery monitoring app it is pretty logical to require it.

## What happened
With Kitkat starting to ship things happened pretty fast and first feedbacks started to emerge reporting that the aforementioned service could not be contacted. 
After some research two changes in 4.4 are responsible for the problem:
1. batteryinfo was renamed to batterystats
2. the BATTERY_STATS permission has been make unavailable to apps (moved from protectionLevel 'dangerous' to 'signature|system'
[This](https://android.googlesource.com/platform/frameworks/base.git/+/3714141d287c965ea3451831dd5167f63f8db87b) is the change preventing apps to be granted that permission: 
As you can see the change refers to an issue number but this issue is not documented in the public tracker (welcome to not-really-open-source).

I have posted on the android-platform forum to [request a clarification](https://groups.google.com/forum/#!topic/android-platform/f-7Td9aeFKY) on why that change was made, but DID NOT GET ANY satisfying feedback. Therefore I have [filed an issue](https://code.google.com/p/android/issues/detail?id=61975&thanks=61975&ts=1383910497) in the public bug tracker for this change to be rolled back.

~~I kindly invite you to upvote this request by staring it (no comment "I vote for this" or "Please undo this" please, but star the issue)~~

**Update 13th of December 2013**
Despite almost 800 votes for that [request](https://code.google.com/p/android/issues/detail?can=2&start=0&num=100&q=&colspec=ID%20Type%20Status%20Owner%20Summary%20Stars&groupby=&sort=&id=61975) to revert the change that revokes normal apps to access BATTERY_STATS has received 800+ votes before being closed by Google with the reason WorkingAsIntended.) it was closed as **WorksAsIntended** by Google without further information.

## Why does all this matter
It matters to me as this change broke my app and forced me to spend a lot of time and effort working around this with some dirty hacks.

It also matters to me because it shows the limit of a platform and an organization that only pretends to be open source:

- the source code was released but the reason for that change was not communicated
- there is no way to file a pull request to the code (filing an issue and hoping that it will be considered is not the same)
- decisions to make changes are not transparent to non-Googlers 

## What could be different
Wouldn't it be great if we could try to change that situation and take control?
A dream would come true if popular Android ROM projects could take control of this kind of changes and decide (or not) to revert them in an open process.

I would really have like to hear what CyanogenMod, OmniROM, Paranoid Android or other large projects or recognized dev had to say about that, but I was never contacted by any ROM developer in this regard.
 

