+++
title =  "k3s: shorter but not less powerful"
tags = ["kubernetes",]
categories = ["Kubernetes",]
draft = true

date = "2019-03-04"
+++

## Introduction

There are a lot of conversations and articles around Rancher's new kubernetes distro for the edge [k3s](https://k3s.io/).
It promises easier installation as well as a smaller footprint so I decided to take it for a spin as a potential replacement for [minikube](https://github.com/kubernetes/minikube) as my dev kubernetes environment.

## Install k3s

### One step installation

The installation is easy:
```
curl -sfL https://get.k3s.io | sh -
```

.... and so is the uninstallation as the installer creates a script for that: 
 ```
/usr/local/bin/k3s-uninstall.sh
```

Once installed you can run commands against the cluster:
```
k3s kubectl get nodes
```

In order to use k3s as a "normal" kubernetes cluster just copy `/etc/rancher/k3s/k3s.yaml` to `.kube/config` or to `.kube/config.d/<your-config-name-here>`. If you use the second setup switching cluster can easily been done by `export KUBECONFIG=~/.kube/config.d/<your-config-name-here>`

If you want to access the cluster over the network you need just to change `clusters.cluster.server` in the kube-config.

### Installation with some tuning

k3s `0.2.0` comes with interesting features to support local development and short feedback-loops:
1. Any manifest copied to `/var/lib/rancher/k3s/server/manifests` will be automatically deployed
1. Any docker image (in docker save format) copied into `/var/lib/rancher/k3s/agent/images` is automatically made available on the cluster

The second option is interesting, as an alternative to minikube's `eval $(minikube docker-env)`.

1. Create the images directory: `sudo mkdir /var/lib/rancher/k3s/agent/images`
1. Restart k3s: `sudo service k3s restart`

Any finally copy a docker image into that directory
```

docker save <some-image> -o <filename>
sudo cp <filename> /var/lib/rancher/k3s/agent/images
```

After a restart (`service stop` + `kill` + `service start`) the image can be seen: `sudo k3s crictl images`



### Other options

## Runtime experience

All tests have been done on my dell xps15 with an i7-6700HQ and 32 GB of RAM, running linux Mint 18.2, with:
- Linux kernel 4.15.0-39-generic
- minikube v0.34.1 with k8s v1.13.3
- k3s v0.2.0 with k8s v1.13.4-k3s.1
- Virtualbox 6.0



### Facts and figures

| Action                     | Time         | Memory  |
| ---------------------------|-------------:|:--------|
| initial `minikube start`   | 4m10.865s    | 3.3GB   |
| subsequent `minikube start`| 2m41.904s    | 1.8GB   |
| k3s install (v0.2.0)       | 1m5.663s     | 212MB   |
| subsequent k3s start (*)   | 0m0.171s     | 92MB    |

(*) `service k3s stop` did not really stop k3s so the processes were killed by hand before the test:
`sudo kill -9 $(ps aux | grep containerd-shim | awk '{ print $2}')`

## Gaps

## Conclusion


## Further readings

- [Introduction to a lightweight Kubernetes distribution build for the edge](https://www.youtube.com/watch?v=5-5t672vFi4)
