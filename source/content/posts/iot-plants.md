+++
title =  "Connected plants"
tags = ["iot","grafana", "rpi"]
categories = ["iot", "rpi"]
draft = false
date = "2020-06-21"
+++

## Introduction

A few weeks ago I had a few days off and, as I will most probably spend my summer vacations in Balconia I started a little non-IT (ahem!) project to set-up my balcony as a green oasis for the summer:

- A little table and two chairs
- A comfortable chair to lie in the sun
- A rack and side-board for plants
- A few plants

As I don't have a green thumb, plants are of course somewhat risky, so I decided that I needed some indicator to help me understand when my plant need water and minerals. This post is about where that brought me.<!--more--> There is a whole ecosystem around plants so I decided to order two devices for testing:

- An analog display that measures humidity, soil Ph and light
- A Bluetooth LE device that measures humidity, light, temperature and soil minerals, and displays it in an app

I could also have gone down the rabbit-hole of DIY [sensoring and watering with Arduino](https://www.instructables.com/id/Arduino-Plant-Monitor/) but I decided not to, at least for now...

<img src="/balcony_screen.jpg"
     alt="My balcony"
     style="margin-left: auto; margin-right: auto; width: 80%;" />

## Devices and ecosystem

### Analog display

<img src="/analog_screen.jpg"
     alt="Analog device"
     style="margin-left: auto; margin-right: auto; width: 50%;" />

The analog device is pretty straight-forward, and it comes without an ecosystem: it has a display and you can switch it between different modes, to display humidity, light and the Ph. It also comes with a sheet where reference values for different plants can be read from. Nothing fancy, it does the job but that's not for me :)

### Bluetooth LE devices

<img src="/ble_screen.jpg"
     alt="Bluetooth LE device"
     style="margin-left: auto; margin-right: auto; width: 50%;" />

The Bluetooth LE devices are pretty expensive, and they come at different price-points on Amazon, depending on how long you are willing to wait.

They come with an [Android](https://play.google.com/store/apps/details?id=com.huahuacaocao.vegtrug&hl=de) and [iOS](https://apps.apple.com/us/app/vegtrug-grow-care/id1417713794) App called "VegTrug Grow Care". Later I learned that these sensors are compatible (knock-offs?) of the Xiaomi MI Home sensors.

The intelligence of course lays in the ecosystem: the app. You use it to activate the sensors but it also comes with a pretty complete database of plants, with the definition of thresholds, historical data and tips on how to take care of your plants: pretty much everything I need...

## Hackability

I think you have guessed it by now: I am all-in with the BLE ecosystem. I have my app all set-up and configured with my plants, so what comes next?

<img src="/android_app_screen.png"
     alt="Android app"
     style="margin-left: auto; margin-right: auto; width: 50%;" />

### Extracting sensor-data

As I mentioned, the key is that the sensors are compatible with the "Xiaomi MI Home" sensors, and guess what? there are some libraries for those, for example the [Python miflora](https://pypi.org/project/miflora/) library.

So it didn't take me long to hack some code together, running on a Raspberry PI 3, to poll the data. The code is open sources under the MIT license and can be found [here](https://github.com/asksven/plant-sensors).

What it does it that it queries the sensors based on a list of the BLE MAC addresses, retrieves the interesting and relevant data (battery, firmware version, temperature, light, moisture, conductivity), and puts it into a time-series database ([InfluxDB](https://www.influxdata.com/)).

#### Database design

Time-series database are a little strange at first, so I will share the schema I use. It can be expressed a a JSON object:

```python
    json_body = [ \
      {"measurement": "plant_sensor", \
      "tags": {"id": item['id'], "plant": item['plant'], "mac": item['mac']}, \
      "time": now, \
      "fields": { \
          "battery": poller.battery_level(), \
          "temperature": poller.parameter_value('temperature'), \
          "light": poller.parameter_value('light'), \
          "moisture": poller.parameter_value('moisture'), \
          "conductivity": poller.parameter_value('conductivity') \
      }}]
```

- The database (`measurement`) is called `plant_sensor`
- The fields are all numeric values: `battery`, `temperature`, `light`, `moisture` and `conductivity`
- The tags (for filtering the data) are an `id`, the name of the `plant`, its `mac` address
- And as it is a time-series `time` should not be missed

The way you should think of time-series databases is:

- put what you will be filtering on in the `where` clause into tags
- the fields are the data you want to represent, i.e. the values
- push the data in the right (time) order to the database, in UTC time. Conversions to local time will happen at the display layer

### Leveraging sensor-data

I am into Kubernetes so it should be no surprise that I run the InfluxDB instance on my Kubernetes cluster, and the same goes for [Grafana](https://grafana.com/) on Kubernetes as well. My home-cluster is composed of 5 Raspberry PI 4, running [k3s](https://k3s.io/) on [Ubuntu 20.04](https://ubuntu.com/blog/ubuntu-20-04-lts-is-certified-for-the-raspberry-pi) 64 bits.

I will not go into setting-up [Grafana](https://github.com/helm/charts/tree/master/stable/grafana) and [InfluxDB](https://github.com/influxdata/helm-charts) in this post as it is pretty straight-forward: I use the official [Helm](https://helm.sh/) charts to install both.

If you are interested in the details of the setup, especially in regards to arm, let me know.

#### Data source

Obviously if you want to query data in Grafana you need a data source. InfluxDB is available out-of-the-box in Grafana so the configuration is just about entering the URL of your InfluxDB database.

#### Queries

To get acquainted with the data I recommend the "Explore" mode in Grafana. You can use it to connect to the data source and query the data that you have uploaded.

For example:

```sql
SELECT "moisture","temperature" FROM "plant_sensor" WHERE ("id" = '2')
```

will show a graph of the moisture and temperature of plant #2:

<img src="/query.png"
     alt="Example query"
     style="margin-left: auto; margin-right: auto; width: 80%;" />

#### Dashboards

I have built two dashboards for my plants:

An overview of each field `moisture`, `temperature`, `light`, and `conductivity`, for all plants:

<img src="/dashboard_2.png"
     alt="Overview dashboard"
     style="margin-left: auto; margin-right: auto; width: 80%;" />

An actionable overview of `moisture` and `conductivity` for each plant:

<img src="/dashboard_1.png"
     alt="Status dashboard"
     style="margin-left: auto; margin-right: auto; width: 80%;" />

#### Alerts

Grafana also comes with alerting so it is pretty easy to send define alerts when thresholds are reached. I do this on `moisture` and `conductivity`. The thresholds come from the "Veg Trug" app, and are specific to each plant.

<img src="/alerts.png"
     alt="Per-plant alerts"
     style="margin-left: auto; margin-right: auto; width: 80%;" />

### Rounding-up the experience

To complete the user experience I have built a physical dashboard, based on a Raspberry PI 3 with a 7" screen.

<img src="/rpi_dash_screen.jpg"
     alt="Raspberry PI dashboard"
     style="margin-left: auto; margin-right: auto; width: 80%;" />

The Rpi runs Raspbian with auto-login, Chromium opening-up at startup, showing the "Plants" dashboard.

Alerts are sent by mail to all family members so that everyone is aware when the plants need to be serviced.

## Shopping-list

1. [Analog sensor](https://www.amazon.de/gp/product/B082B39YW8?ie=UTF8&tag=asksven-21&camp=1638&linkCode=xm2&creativeASIN=B082B39YW8)
1. [BLE sensor](https://www.amazon.de/gp/product/B0851FBJ3C?ie=UTF8&tag=asksven-21&camp=1638&linkCode=xm2&creativeASIN=B0851FBJ3C)
1. [7" screen with Raspberry PI 3 bundle](https://www.amazon.de/gp/product/B01M0AT5O5?ie=UTF8&tag=asksven-21&camp=1638&linkCode=xm2&creativeASIN=B01M0AT5O5)
1. [Power adapter for Raspberry PI 3](https://www.amazon.de/gp/product/B01566WOAG?ie=UTF8&tag=asksven-21&camp=1638&linkCode=xm2&creativeASIN=B01566WOAG)
1. [64GB microSD card](https://www.amazon.de/gp/product/B073JYVKNX?ie=UTF8&tag=asksven-21&camp=1638&linkCode=xm2&creativeASIN=B073JYVKNX)

## Future

For the future I have plans to:

- add more data to the dashboard, e.g. the data I collect from the [Mobile Alerts](https://github.com/asksven/mobile-alerts-scraper) sensors by Techtronic. Fortunately these are hackable too ;)
