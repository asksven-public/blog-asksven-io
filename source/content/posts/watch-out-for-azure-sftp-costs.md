+++
title =  "Watch-out for Azure STFP costs"
tags = ["cloud","azure"]
categories = ["cloud"]
draft = false

date = "2023-08-13"
+++


## Introduction

I run backups for both my Kubernetes clusters and my Linux servers. Since I want offsite backups and have an Azure subscriptions I follow these approaches:

- my Kubernetes backup use a Minio proxy connected to an Azure storage account, with a container per cluster/app
- my Linux backup use(d) sftp to an Azure storage account with SFTP enabled, with a container per server 

Now what is the issue with this?

## Cost overview

After a few days I saw a spike in my Azure costs. Of course I was expecting a small increase for the storage but not an increase of an order of magnitude. In the Cost analysis in the Azure portal I noticed the highest cost for an item in the Tier `Blob Features` of the Service family `Blob Features - SSH File Transfer Protocol LRS - EU West` and with the Meter `Hour Enabled`. Digging further in the [Azure Blob Storage pricing](https://azure.microsoft.com/en-gb/pricing/details/storage/blobs/) I found that for my region (west-europe) SSH File Transfer Protocol (SFTP) costs 28 cent per hour, that's roundabout 200€ per month!

## Cost optimization

There are two options to save these costs:

1. Turn on/off SFTP on the storage account before/after the backup, using the Azure CLI
2. Use `azcopy` instead

I went for option 2 with a SAS token per container because it makes my life easier: `azcopy sync` has an option `--delete-destination=true` so I manage retention with this option combined with removing local backups using `find /<backup-location>/ -type f -mtime +<retention> -name '*.gz' -delete`.