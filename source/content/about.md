+++
title = "About me"
hidden = true
+++

I was born in Sweden in 1967, grew up and studied in France and currently live in Germany. In a nutshell, I am European.

I love Android, Linux, containers, Kubernetes and many other topics related to technology. I am the author of Better Battery Stats (Android), that can be found on the Google Play Store. Most of what I do is open-sourced so check my Github and Gitlab links for more.

In my day job I am a solution architect and am serving a team that covers a broad scope of technologies, e.g. data center infrastructure, cloud, containers, netorking, web and mobile devlopment, mobile device management, M365.
We also take end-to-end responsibility for a set of platforms that enable our customers to deliver business value. 

This site is an unstructured collection of things I have been working on or am interested in.

The opinions expressed here are my own and may not reflect the views or opinions of my employer.
